<!DOCTYPE html>
<html>
<head>
<title>CSRF Demo</title>

<link rel="stylesheet" type="text/css" href="./resources/global_styles.css">
</head>

<body>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '2172678392773626',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.1'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>

<div class="centerx">
<form action="#" method="POST" class="centerx marginTop greenBorder">
  <fb:login-button scope="public_profile,email,user_birthday" onlogin="checkLoginState();">
  </fb:login-button>
  </form>
</div>

<div class="greenBorder">
Your Birthday is : <b>
<p id="birthday"> Waiting for auth </p> </b>
</div>

<script>

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  function statusChangeCallback(response){
      console.log(response);
      FB.api('/me', {fields: 'birthday'}, function(response) {
          console.log(JSON.stringify(response));
          document.getElementById('birthday').innerHTML = response.birthday;
     })
  }
}

</script>


</body>

</html>